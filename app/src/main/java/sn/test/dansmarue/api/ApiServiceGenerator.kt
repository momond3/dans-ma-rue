package sn.test.dansmarue.api

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import sn.test.dansmarue.BuildConfig
import java.io.IOException
import java.net.UnknownHostException
import java.util.concurrent.TimeUnit

object ApiServiceGenerator {

    private val httpClient = OkHttpClient.Builder()
    private val builder = Retrofit.Builder()
        .baseUrl("https://opendata.paris.fr/api/" )
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())

    fun <S> createService(serviceClass: Class<S>): S {

        httpClient.addInterceptor { chain ->
            val original = chain.request()
            val request = original.newBuilder()
                .method(original.method(), original.body())
                .build()
            chain.proceed(request)
        }

        if (BuildConfig.DEBUG) {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            httpClient.addInterceptor(logging)
        }

        val retrofit = builder.client(
            httpClient
                .connectTimeout(120, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .build()
        ).build()
        return retrofit.create(serviceClass)
    }

    fun parseError(response: Response<*> ):  ApiError {
        val converter = builder.client(httpClient.build()).build()
            .responseBodyConverter<ApiError>(ApiError::class.java, arrayOfNulls(0))

        val error : ApiError

        try {
            error = converter.convert(response.errorBody()!!)!!
        } catch (e: IOException) {
            return ApiError("Unknown Error")
        } catch (e: Exception) {
            return ApiError("Unknown Error")
        }
        return error
    }
}