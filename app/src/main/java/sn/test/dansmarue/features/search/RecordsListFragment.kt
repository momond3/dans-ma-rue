package sn.test.dansmarue.features.search


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.orhanobut.logger.Logger
import org.koin.androidx.viewmodel.ext.android.getViewModel
import sn.test.dansmarue.R
import sn.test.dansmarue.databinding.FragmentRecordsListBinding
import sn.test.dansmarue.entity.Record
import sn.test.dansmarue.entity.Resource
import sn.test.dansmarue.utils.ApplcationConst
import sn.test.dansmarue.utils.ConnectionUtils

/**
 * A simple [Fragment] subclass.
 *
 */
class RecordsListFragment : Fragment() {

    private lateinit var binding: FragmentRecordsListBinding
    private lateinit var mViewModel: RecordsViewModel
    private lateinit var mAdapter: RecordsListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = getViewModel()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_records_list, container, false )
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupSwipeRefresh()
        setupRecyclerView()
        observeViewModel()
    }


    /**
     * Define Swipe Refresh colors and action to do when
     * user swipe bottom on the screen
     */
    private fun setupSwipeRefresh() {
        binding.swipeRefresh.apply {
            setColorSchemeResources(R.color.colorPrimary, R.color.sw1, R.color.sw2 )
            setOnRefreshListener {
                observeViewModel()
            }
        }
    }


    /**
     * Setup the RecyclerView by defining  an orientation and the adapter for the list
     */
    private fun setupRecyclerView() {
        mAdapter = RecordsListAdapter(listOf()) { field -> onItemClicked(field) }
        binding.recordsList.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = mAdapter
        }
    }


    /**
     * Observe the list from the view model and choose action based on the result
     */
    private fun observeViewModel() {
        if (ConnectionUtils.isConnected(context!!)) {
            mViewModel.getRecordsList().observe(this, Observer {
                binding.swipeRefresh.isRefreshing = false
                when (it?.status) {
                    Resource.LOADING -> {
                        binding.apply {
                            recordsListLoader.visibility = View.VISIBLE
                            recordsList.visibility = View.GONE
                        }
                    }
                    Resource.SUCCESS -> {
                        binding.apply {
                            recordsListLoader.visibility = View.GONE
                            recordsList.visibility = View.VISIBLE
                            it.data?.let { list ->
                                mAdapter.setData(list)
                            } ?: mAdapter.setData(listOf())

                        }
                    }
                    Resource.ERROR -> {
                        Logger.e("Errror ${it.message}")
                        binding.apply {
                            recordsListLoader.visibility = View.GONE
                            recordsList.visibility = View.GONE
                        }
                        Toast.makeText(context, "Erreur de récuperation de  la list ${it.message}", Toast.LENGTH_SHORT)
                            .show()
                    }
                }
            })
        } else {
            binding.recordsListLoader.visibility = View.GONE
            Toast.makeText(context, "Vous n'avez pas de connection internet", Toast.LENGTH_SHORT)
                .show()
        }
    }


    /**
     * Method called when an item in the list is clicked, this will open the detail page
     * @param field : the item clicked by the user
     */
    private fun onItemClicked(field: Record) {
        startActivity(Intent(activity, RecordDetailActivity::class.java).apply {
            putExtra(ApplcationConst.RECORD_OBJECT_KEY, field)
        })
    }
}