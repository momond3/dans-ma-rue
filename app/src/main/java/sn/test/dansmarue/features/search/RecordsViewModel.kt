package sn.test.dansmarue.features.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import sn.test.dansmarue.entity.Record
import sn.test.dansmarue.entity.Resource

class RecordsViewModel(private val recordsRepo: RecordsRepository): ViewModel() {

    fun getRecordsList() : LiveData<Resource<List<Record>>> = recordsRepo.getRecords(dataSet, lang, facet1, facet2, facet3)

    companion object {
        const val  dataSet = "dans-ma-rue"
        const val lang = "fr"
        const val facet1 = "type"
        const val facet2 = "ville"
        const val facet3 = "arrondissement"
    }
}