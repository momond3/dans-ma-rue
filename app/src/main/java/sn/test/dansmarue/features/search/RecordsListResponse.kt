package sn.test.dansmarue.features.search

import sn.test.dansmarue.entity.FacetGroup
import sn.test.dansmarue.entity.Parameters
import sn.test.dansmarue.entity.Record

data class RecordsListResponse(
    val facet_groups: List<FacetGroup>,
    val nhits: Int,
    val parameters: Parameters,
    val records: List<Record>)
