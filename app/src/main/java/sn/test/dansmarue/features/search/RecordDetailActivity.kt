package sn.test.dansmarue.features.search

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import sn.test.dansmarue.R
import sn.test.dansmarue.databinding.ActivityRecordDetailBinding
import sn.test.dansmarue.entity.Record
import sn.test.dansmarue.utils.ApplcationConst

class RecordDetailActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var recordDetailBinding: ActivityRecordDetailBinding
    private var currentRecord: Record?  = null
    private lateinit var mapView: MapView
    private lateinit var map: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        recordDetailBinding = DataBindingUtil.setContentView(this, R.layout.activity_record_detail)
        setSupportActionBar(recordDetailBinding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        currentRecord = intent?.getParcelableExtra(ApplcationConst.RECORD_OBJECT_KEY)
        setView()
        setMap()
    }

    /**
     * Set the detail of clicked record
     */
    private fun setView() {
        currentRecord?.fields?.let { field->
            recordDetailBinding.contentRecordDetail.apply {
                recordType.text = field.type
                recordSubType.text = field.soustype
                recordCity.text = field.ville
                recordAddress.text  = field.adresse
                recordDate.text = field.datedecl
                recordCouncil.text = field.conseilquartier
                recordInterferer.text  = field.intervenant
            }
        }
    }


    /**
     * Configure and show map based on latitude and longitude in geo_point_2d from the api
     */
    private fun setMap() {
        mapView  = recordDetailBinding.contentRecordDetail.mapView
        with(mapView) {
            onCreate(null)
            getMapAsync(this@RecordDetailActivity)
        }

    }

    /**
     * CallBack when the map is ready
     * @param p0 : Google map object
     */
    override fun onMapReady(p0: GoogleMap?) {
        map = p0 ?: return
        val latitude = currentRecord?.fields?.geo_point_2d!![0]
        val longitude = currentRecord?.fields?.geo_point_2d!![1]
        with(map) {
            if (!::map.isInitialized) return
            val latLng = LatLng(latitude, longitude)
            if (latLng.latitude == 0.0 && latLng.longitude == 0.0) {
            } else {
                animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18f))
                addMarker(
                    MarkerOptions()
                        .position(latLng)
                        .title(currentRecord?.fields?.adresse))?.showInfoWindow()
                mapType = GoogleMap.MAP_TYPE_NORMAL
            }
        }
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView.onDestroy()
    }


    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return true
    }
}
