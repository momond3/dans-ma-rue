package sn.test.dansmarue.features.search

import androidx.lifecycle.MutableLiveData
import com.orhanobut.logger.Logger
import kotlinx.coroutines.*
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import sn.test.dansmarue.api.ApiServiceGenerator
import sn.test.dansmarue.api.ApiUrls
import sn.test.dansmarue.entity.Record
import sn.test.dansmarue.entity.Resource
import kotlin.coroutines.CoroutineContext

object RecordsRepository {
    fun getRecords( dataset: String, lang: String, facet1:String,
                    facet2: String, facet3:String) : MutableLiveData<Resource<List<Record>>> {
        val fields : MutableLiveData<Resource<List<Record>>> = MutableLiveData()
        fields.value  = Resource.loading(null)

        mainScope.launch {
            val client = RecordsListAPI().getRecordService()
            val request = withContext(bgContext) { client.getRecordList(dataset,lang, facet1, facet2, facet3).await() }
            if (request.isSuccessful) {
                fields.value = Resource.success(request.body()?.records)
            } else {
                val errorMessage = ApiServiceGenerator.parseError(request).error
                fields.value = Resource.error(errorMessage, request.code())
            }
        }
        return fields
    }
}

class RecordsListAPI {

    fun getRecordService() : RecordsListService = ApiServiceGenerator.createService(RecordsListService::class.java)

    interface RecordsListService {
        @GET(ApiUrls.SEARCH)
        fun getRecordList(
            @Query("dataset") dataSet: String,
            @Query("lang") lang: String,
            @Query("facet") type: String,
            @Query("facet") ville: String,
            @Query("facet") arrondissement: String
        ): Deferred<Response<RecordsListResponse>>
    }
}

val mainScope = CoroutineScope(Dispatchers.Main + SupervisorJob())
val bgContext: CoroutineContext = Dispatchers.IO + CoroutineExceptionHandler { _, e ->
    Logger.e("CoroutineExceptionHandler $e")

}
