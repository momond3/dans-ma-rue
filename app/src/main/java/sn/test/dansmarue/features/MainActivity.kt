package sn.test.dansmarue.features

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import sn.test.dansmarue.R
import sn.test.dansmarue.databinding.ActivityMainBinding
import sn.test.dansmarue.features.search.RecordsListFragment

class MainActivity : AppCompatActivity() {
    private lateinit var mainBinding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        setSupportActionBar(mainBinding.toolbar)
        replaceFragment()

    }


    /**
     * Method for add fragment in the activity
     */
    private fun replaceFragment() {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.main_container, RecordsListFragment())
            .commitAllowingStateLoss()
    }
}
