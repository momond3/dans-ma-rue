package sn.test.dansmarue.features.search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import sn.test.dansmarue.R
import sn.test.dansmarue.databinding.FieldItemBinding
import sn.test.dansmarue.entity.Record


/**
 * The adapter for the record list,
 * @param recordList : list  to display
 * @param itemClicked: method called when user clicked on an item
 *
 */
class RecordsListAdapter(var recordList: List<Record>?, private val itemClicked: (Record) -> Unit): RecyclerView.Adapter<RecordsListAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val  view = LayoutInflater.from(parent.context).inflate(R.layout.field_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = recordList?.size ?: 0

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val record = recordList!![position]

        holder.itemBinding.apply {
            anomalyType.text = record.fields.type
            addressName.text = String.format(": %s",record.fields.adresse)
            root.setOnClickListener { itemClicked(record) }
        }

    }


    fun setData(newList: List<Record>) {
        recordList = newList
        notifyDataSetChanged()
    }


    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        var itemBinding: FieldItemBinding = DataBindingUtil.bind(itemView)!!
    }
}