package sn.test.dansmarue.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GeoShape(
    val coordinates: List<Double>,
    val type: String
): Parcelable