package sn.test.dansmarue.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

//todo  add nullable
@Parcelize
data class Fields(
    val adresse: String,
    val anneedecl: String,
    val arrondissement: Int,
    val code_postal: String,
    val conseilquartier: String,
    val datedecl: String,
    val geo_point_2d: List<Double>,
    val geo_shape: GeoShape,
    val intervenant: String,
    val moisdecl: Int,
    val numero: Double,
    val objectid: Int,
    val prefixe: String,
    val soustype: String,
    val type: String,
    val ville: String
):Parcelable