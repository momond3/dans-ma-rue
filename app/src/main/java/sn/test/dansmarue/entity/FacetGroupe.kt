package sn.test.dansmarue.entity


data class FacetGroup(
    val facets: List<Facet>,
    val name: String
)