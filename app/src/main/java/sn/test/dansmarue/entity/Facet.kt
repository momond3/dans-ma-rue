package sn.test.dansmarue.entity

data class Facet(
    val count: Int,
    val name: String,
    val path: String,
    val state: String
)