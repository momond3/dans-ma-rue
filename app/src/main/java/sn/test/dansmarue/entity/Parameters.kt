package sn.test.dansmarue.entity

data class Parameters(
    val dataset: List<String>,
    val facet: List<String>,
    val format: String,
    val lang: String,
    val rows: Int,
    val timezone: String
)
