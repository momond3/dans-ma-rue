package sn.test.dansmarue.application

import android.app.Application
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import sn.test.dansmarue.BuildConfig
import sn.test.dansmarue.di.appModule

class DansMaRueApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
        initKoin()
        initLogger()
    }

    /**
     * Initialise Logger, library for pretty logs
     */
    private fun initLogger() {
        Logger.addLogAdapter(object :  AndroidLogAdapter() {
            override fun isLoggable(priority: Int, tag: String?): Boolean {
                return BuildConfig.DEBUG
            }
        })
    }

    /**
     * Initialise Koin, a library for dependency injection
     */
    private fun initKoin() {
        startKoin { androidContext(this@DansMaRueApplication)
        modules(appModule)}

    }

    companion object {
        lateinit var instance: DansMaRueApplication
    }
}