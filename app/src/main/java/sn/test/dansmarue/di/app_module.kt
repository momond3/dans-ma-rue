package sn.test.dansmarue.di

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module
import sn.test.dansmarue.features.search.RecordsRepository
import sn.test.dansmarue.features.search.RecordsViewModel


var viewModelModule : Module = module {
    viewModel { RecordsViewModel(get()) }
}
var repositorylModule : Module = module {
    single { RecordsRepository }
}

val appModule = listOf(viewModelModule, repositorylModule)